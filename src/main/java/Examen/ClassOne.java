package Examen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ClassOne {
    public JFrame framePP;
    public JPanel panelPP;
    public JTextField field1;
    public JTextField field2;
    public JButton button;
    public JLabel title;
    PageController control = new PageController(this);

    public ClassOne() {
        initialize();
    }

    public void initialize() {
        framePP = new JFrame();
        framePP.setSize(700, 600);
        framePP.setBounds(450, 550, 650, 550);
        framePP.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        framePP.getContentPane().setLayout(null);
        panelPP = new JPanel();
        panelPP.setBounds(0, 0, 700, 600);
        panelPP.setBackground(Color.darkGray);
        framePP.getContentPane().add(panelPP);
        panelPP.setLayout(null);
        setFields();
        framePP.add(panelPP);
        framePP.setVisible(true);
    }

    public void setFields() {
        title = new JLabel(" Examen ");
        title.setBounds(225, 20, 200, 100);
        title.setFont(new Font("Times New Roman", Font.PLAIN, 28));
        title.setForeground(Color.PINK);
        panelPP.add(title);

        button = new JButton("Button");
        button.setBounds(100, 400, 200, 50);
        button.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        button.setBackground(Color.PINK);
        button.addActionListener(control);
        panelPP.add(button);

        field2 = new JTextField();
        field2.setBounds(160, 250, 250, 70);
        field2.setBackground(Color.PINK);
        field2.setEditable(false);
        JLabel label_1 = new JLabel("Field 2: ");
        label_1.setBounds(20, 250, 100, 30);
        label_1.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        label_1.setForeground(Color.PINK);
        panelPP.add(label_1);
        panelPP.add(field2);

        field1 = new JTextField();
        field1.setBounds(160, 150, 250, 70);
        field1.setBackground(Color.PINK);
        JLabel label_2 = new JLabel("Field 1: ");
        label_2.setBounds(20, 150, 100, 30);
        label_2.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        label_2.setForeground(Color.PINK);
        panelPP.add(label_2);
        panelPP.add(field1);
    }

    public JFrame getFrameP() {
        return framePP;
    }

    public void setFrameP(JFrame frameP) {
        this.framePP = frameP;
    }

    public JPanel getPanelP() {
        return panelPP;
    }

    public void setPanelP(JPanel panelP) {
        this.panelPP = panelP;
    }

    public JTextField getField1() {
        return field1;
    }

    public void setField1(JTextField field1) {
        this.field1 = field1;
    }

    public JTextField getField2() {
        return field2;
    }

    public void setField2(JTextField field2) {
        this.field2 = field2;
    }

    public JButton getButton() {
        return button;
    }

    public void setButton(JButton button) {
        this.button = button;
    }
}

class PageController implements ActionListener {
    ClassOne page;

    public PageController(ClassOne page) {
        this.page = page;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == page.getButton()) {
            String s = page.getField1().getText();
            page.getField2().setText(s);
        }
    }
}

class Main {
    public static void main(String[] args) {
        ClassOne classOne = new ClassOne();
    }
}
