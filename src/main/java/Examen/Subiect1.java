package Examen;

public class Subiect1 {

}

interface Y {
    public void f();
}

class I implements Y {
    private long t;
    private K k; //asociere

    @Override
    public void f() {

    }
}

class J {
    public void i(I I) {

    }
}

class K {
    M m;
    L l = new L();

    public K(M m) {
        this.m = m;
    }
}

class L {
    public void A() {
    }
}

class M {
    public void B() {
    }

}

class N {
    I i;//asociere
}